﻿/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;
using System.Collections;
using System;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {      
        #region PRIVATE_MEMBER_VARIABLES

        TrackableBehaviour mTrackableBehaviour;

        bool isFirst = true;

        string chosenPromo;

        GameObject panel;

        Text text;

        PromoList promoList;

        bool isDisplaying = false;
                
        #endregion // PRIVATE_MEMBER_VARIABLES

        #region UNTIY_MONOBEHAVIOUR_METHODS        

        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }

            promoList = GameObject.FindObjectOfType<PromoList>();

            Canvas canvas = GameObject.FindObjectOfType<Canvas>();

            panel = canvas.transform.FindChild("TextPanel").gameObject;

            text = panel.GetComponentInChildren<Text>(true);                  
        }
       
        #endregion // UNTIY_MONOBEHAVIOUR_METHODS

        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

            if (isFirst)
            {
                chosenPromo = promoList.getPromo();
                
                StartCoroutine(display(panel, "Promoção descoberta",  2.5));

                isFirst = false;
            }
            else if(isDisplaying == false)
            {
                StartCoroutine(display(panel, "Promoção já encontrada", 2.5));
            }      
        }

        private IEnumerator display(GameObject obj, string title, double seconds)
        {
            isDisplaying = true;

            text.text = title;
                        
            obj.SetActive(true);            
            yield return new WaitForSeconds(Convert.ToSingle(seconds));

            text.text = chosenPromo;

            yield return new WaitForSeconds(Convert.ToSingle(seconds));

            obj.SetActive(false);

            isDisplaying = false;
        }

        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }

        #endregion // PRIVATE_METHODS
    }
}
