﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromoList : MonoBehaviour
{
    public List<string> inspectorPromoList;

    static System.Random rnd;

    static PromoList()
    {
        rnd = new System.Random();
    }
    // Use this for initialization
    void Start () {}
	
	// Update is called once per frame
	void Update () {}

    public string getPromo()
    {
        int i = rnd.Next(inspectorPromoList.Count);

        string chosenPromo = inspectorPromoList[i];

        inspectorPromoList.RemoveAt(i);

        return chosenPromo;
    }
}